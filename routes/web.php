<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'tasks'], function (){
    Route::get('/listAll','TaskController@listAll')
        ->name('tasks.listAll');
    Route::get('/listTasksAjax','TaskController@listTasksAjax')
        ->name('tasks.listTasksAjax');
    Route::get('/updatePriority','TaskController@updatePriority')
        ->name('tasks.updatePriority');
    Route::get('/user/{id}','TaskController@user')
        ->name('tasks.user');
    Route::get('/create','TaskController@create')
        ->name('tasks.create');
    Route::post('/store','TaskController@store')
        ->name('tasks.store');
    Route::get('/edit','TaskController@edit')
        ->name('tasks.edit');
    Route::put('/update/{task}','TaskController@update')
        ->name('tasks.update');
    Route::delete('/delete/{task}','TaskController@destroy')
        ->name('tasks.delete');
});
