require('./bootstrap');

window.addTasks =  function(tasks_list_route){
    //Enviamos una solicitud con el ruc de la empresa
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: tasks_list_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{},
        success: function (data) {
            //console.log(data);
            $(data).each(function (index,value) {
                //console.log("--------STAGE OPP "+stage_id+"--------");
                //CLONE THE TEMPLATE
                let cloned_template = $("#task-template .task-wrapp").clone();
                //FILL TEMPLATE WITH DATA
                cloned_template.attr("data-id",value.id);
                cloned_template.attr("data-priority",value.priority);
                cloned_template.attr("data-project",value.project_id);
                cloned_template.find(".edit-task").attr("data-id", value.id);
                cloned_template.find(".delete-task").attr("data-id", value.id);
                cloned_template.find(".task-single-title").html(value.name);
                cloned_template.find(".task-single-project").html(value.project.name);
                cloned_template.find(".task-single-due").html(value.due_to);
                //ADD ELEMENT TO STAGE LIST
                $(".tasks-stage-body").append(cloned_template);
                //ADD SORTABLE FUNCTIONALITY TO ELEMENT
                makeSortable();
                //console.log(value);
            });
            //console.log("-----------------STAGE "+stage_id+"----------------");
        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });
};


window.makeSortable = function() {

    $(".tasks-stage-body").sortable({
        connectWith: $(".tasks-stage-body"),
        delay: 150,
        stop: function( event, ui ) {
            //make updated opportunity consistent
            //RETRIEVE THE ITEM MOVED
            let item_moved = ui.item;
            //update values on db
            updatePriorityTask(item_moved);
            //update values locally
            updateValuesLocally();

        }
    });

};

window.updatePriorityTask = function(item_moved) {
    //GET INFORMATION OF TASK
    let task = item_moved.attr("data-id");
    let priority = item_moved.index()+1;
    console.log("new priority: "+priority);
    console.log("Task: "+task);

    //UPDATING TASK PRIORITY IN AJAX CALL
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: priority_update_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            task: task,
            priority: priority,
        },
        success: function (data) {
            console.log("Se actualizó la oportunidad con éxito.");
        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }else{
                alert("Ocurrió un error, por favor cargue la página e intente de nuevo la operación.")
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });


};
window.editTask = function(task, edit_route) {
    $("#modalEditTask .modal-ajax-content").html("Loading data...");

    //GETTING THE TASK EDIT MODAL
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: edit_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            task: task,
        },
        success: function (data) {
            $("#modalEditTask .modal-ajax-content").html(data.html);
            //console.log(data);
        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }else{
                $("#modalEditTask .modal-ajax-content").html(data.responseJSON.error);
            }
            console.log(data);
            console.log(data.responseJSON.error);
            console.log("-----------------ERROR----------------");
        }
    });


};

window.deleteTasks =  function(task){
    delete_route_with_task = delete_route.replace('replace', task);
    console.log(task);
    console.log(delete_route_with_task);
    $.ajax({
        type: 'DELETE', //THIS NEEDS TO BE GET
        url: delete_route_with_task,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            "_token": csrf,
        },
        success: function (data) {
            console.log("task deleted.");
            location.reload();

        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }else{
                alert(data.error)
            }
            console.log(data);
            console.log(data.error);
            console.log("-----------------ERROR----------------");
        }
    });
};

window.updateValuesLocally = function() {
    console.log("updated locally");

}
