
<form
    method="POST"
    action="{{ route('tasks.update', $task->id) }}"
    novalidate
>
    @method('PUT')
    @csrf

    <div class="form-group">
        <label for="name">Name</label>
        <input
            type="text"
            class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
            name="name"
            id="name"
            value="{{ old('name') ?: $task->name  }}"
        >
        @if($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <label for="priority">Priority</label>
        <input
            type="number"
            class="form-control {{ $errors->has('priority') ? 'is-invalid': '' }}"
            name="priority"
            id="priority"
            value="{{ old('priority') ?: $task->priority }}"
        >
        @if($errors->has('priority'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('priority') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="due_to_update">Due to</label>
        <div class="input-group date" id="due_to_up_tp" data-target-input="nearest">
            <input
                type="text"
                name="due_to"
                id="due_to_update"
                class="form-control datetimepicker-input"
                data-target="#due_to_up_tp"
                data-toggle="datetimepicker"
                autocomplete="off"
                value="{{ old('due_to') ?: $task->due_to }}"
            />
            <div class="input-group-append" data-target="#due_to_up_tp" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="project_id">Project</label>
        <select
            class="form-control {{ $errors->has('project_id') ? 'is-invalid': '' }}"
            name="project_id"
            id="project_id"
            required
        >
            @foreach(\App\Project::pluck('name', 'id') as $id => $name)
                <option
                    @if(old('project_id'))
                    {{ (int) old('project_id') === $id ? 'selected' : '' }}
                    @elseif($task->id)
                    {{ (int) $task->project_id === $id ? 'selected' : '' }}
                    @endif
                    value="{{ $id }}"
                >{{ $name }}</option>
            @endforeach
        </select>
    </div>


    <div class="form-group">
        <button type="submit" class="btn btn-thales-secondary">
            {{ __("Edit") }}
        </button>
    </div>

</form>
<script>
    $('#due_to_up_tp').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'en',
        stepping: 15,
        sideBySide: true,
        showClose: true
    });
</script>
