
<div class="modal" id="modalAddTask">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add task</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form
                    method="POST"
                    action="{{ route('tasks.store') }}"
                    novalidate
                >
                    @csrf

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input
                            type="text"
                            class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                            name="name"
                            id="name"
                            value="{{ old('name') ?: '' }}"
                        >
                        @if($errors->has('name'))
                            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="priority">Priority</label>
                        <input
                            type="number"
                            class="form-control {{ $errors->has('priority') ? 'is-invalid': '' }}"
                            name="priority"
                            id="priority"
                            value="{{ old('priority') ?: '1' }}"
                        >
                        @if($errors->has('priority'))
                            <span class="invalid-feedback">
                <strong>{{ $errors->first('priority') }}</strong>
            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="due_to">Due to</label>
                        <div class="input-group date" id="due_to_tp" data-target-input="nearest">
                            <input
                                type="text"
                                name="due_to"
                                id="due_to"
                                class="form-control datetimepicker-input"
                                data-target="#due_to_tp"
                                data-toggle="datetimepicker"
                                autocomplete="off"
                            />
                            <div class="input-group-append" data-target="#due_to_tp" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="project_id">Project</label>
                        <select
                            class="form-control {{ $errors->has('project_id') ? 'is-invalid': '' }}"
                            name="project_id"
                            id="project_id"
                            required
                        >
                            @foreach(\App\Project::pluck('name', 'id') as $id => $name)
                                <option
                                    @if(old('project_id'))
                                    {{ (int) old('project_id') === $id ? 'selected' : '' }}
                                    @endif
                                    value="{{ $id }}"
                                >{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-thales-secondary">
                            {{ __("Add") }}
                        </button>
                    </div>

                </form>

            </div>

        </div>
    </div>
</div>

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script>
        $(function () {
            $('#due_to_tp').datetimepicker({
                format: 'YYYY-MM-DD HH:mm',
                locale: 'en',
                stepping: 15,
                sideBySide: true,
                showClose: true
            });
        });

    </script>
@endpush
