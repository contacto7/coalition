@extends('layouts.app', ['page' => 'opportunitties'])

@section('content')
    <div class="container container-dashboard-opp">
        <div class="row justify-content-center" style="background-color: inherit">
            <div class="tasks-wrapp">
                <div class="tasks-inn">

                    <!-- TASKS BODY -->
                    <div class="tasks-body-wrapp">
                        <div class="tasks-body-inn">

                            <!-- TASKS STAGE -->
                            <div class="tasks-stage-wrapp">
                                <div class="tasks-stage-inn">
                                    <!-- TASKS STAGE TITLE-->


                                    @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="task-project" >Select projet</label>
                                        <select class="form-control task-project" name="task-project" id="task-project">
                                            <option
                                                value="0"
                                            >All</option>
                                            @foreach(\App\Project::pluck('name', 'id') as $id=>$name)
                                                <option
                                                    value="{{ $id }}"
                                                >{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        <button
                                            type="button"
                                            class="btn btn-primary btn-add-opp-db mt-3"
                                            data-toggle="modal"
                                            data-target="#modalAddTask"
                                            id="btnAddTasksModal"
                                        >
                                            Add task
                                        </button>

                                    </div>
                                    <!-- TASKS STAGE BODY-->

                                    <div class="tasks-stage-body">
                                        <!-- TASKS LIST-->
                                        <!-- FILLED WITH AJAX -->
                                        <!-- FROM TEMPLATE WITH ID = task-template -->
                                        <!-- WITH FUNCTION addTasks in app.js  -->


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div id="task-template" class="d-none">
            <!-- TASK TEMPLATE WRAPP-->
            <div class="task-wrapp">
                <div class="task-inn">

                    <div class="card border-secondary mb-3" style="max-width: 18rem;">
                        <div class="card-header">
                            <span class="task-single-title">NAME </span>
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                Due to: <span class="task-single-due">created</span>
                            </div>
                            <div class="card-text">
                                <i class="task-single-project">PROJECT</i>
                            </div>
                            <div class="card-text">
                                <button
                                    type="button"
                                    class="btn btn-sm btn-outline-secondary edit-task"
                                    data-toggle="modal"
                                    data-target="#modalEditTask"
                                    id="btnAddTasksModal"

                                >
                                    Edit <i class="fa fa-pencil"></i>
                                </button>
                                <button
                                    type="button"
                                    class="btn btn-sm btn-outline-secondary delete-task"
                                    data-href="{{ route("tasks.delete", "replace") }}"
                                    id="btnAddTasksModal"

                                >
                                    Delete <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>


    <!-- MODAL TO ADD NEW TASKS -->
    <!-- The Modal -->
    @include('partials.modals.modalAddTask')

    <!-- MODAL TO EDIT TASKS -->
        <!-- The Modal -->
        <div class="modal" id="modalEditTask">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">EDIT TASK</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>

        //MODELS AJAX ROUTES
        let tasks_list_route = "{{ route('tasks.listTasksAjax') }}";
        let priority_update_route = "{{ route('tasks.updatePriority') }}";
        let delete_route = "{{ route('tasks.delete', "replace") }}";
        let edit_route = "{{ route('tasks.edit') }}";
        let csrf ="{{ csrf_token() }}";


        $(document).ready(function() {
            addTasks(tasks_list_route);
        });

        //SHOW ONLY TASKS FOR PROJECT SELECTED
        $(document).on("change", "#task-project", function(){
            var project_id = this.value;
            //IF PROJECT SELECTED IS ALL (VALUE 0) SHOW ALL TASKS
            //ELSE, SHOW TASKS FOR PROJECT
            if(project_id==0){
                $(".task-wrapp").show();
            }else{
                $(".task-wrapp").hide();
                $(".task-wrapp[data-project="+project_id+"]").show();
            }

        });
        //EDIT TASK
        $(document).on("click", ".edit-task", function(){
            var task = $(this).attr("data-id");

            editTask(task, edit_route);

        });
        //DELETE TASK
        $(document).on("click", ".delete-task", function(){
            var task = $(this).attr("data-id");
            var delete_route = $(this).attr("data-href");

            if(confirm("Are you sure to delete the task?")){
                deleteTasks(task);
            }

        });


    </script>
@endpush
