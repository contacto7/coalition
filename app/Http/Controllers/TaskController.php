<?php

namespace App\Http\Controllers;

use App\f;
use App\Http\Requests\TaskRequest;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll()
    {
        //POLICIES
        //$canListTasks = auth()->user()->can('viewAny', Task::class );
        $canListTasks = true;
        //END POLICIES

        if(!($canListTasks) ){
            return redirect('/')->with('modalMessage',['Alert', __("You cannot view tasks.")]);
        }

        $tasks = Task::with(['project'])
            ->orderBy('priority', 'asc')
            ->get();

        $tasks = Task::get();

        //dd($vouchers);
        return view('tasks.listAll' , compact('tasks') );
    }

    public function listTasksAjax(){
        //POLICIES
        //$canListTasks = auth()->user()->can('viewAny', Task::class );
        $canListTasks = true;
        //END POLICIES

        if( ! ($canListTasks) ){
            return response()->json(['error' => 'Not authorized.'],403);
        }

        $tasks = Task::with([
            'project'
        ])
            ->orderBy('priority', 'asc')
            ->get();

        return $tasks->toJson();
    }

    public function updatePriority(Request $request){
        //POLICIES
        //$canUpdateTasks = auth()->user()->can('update', Task::class );
        $canUpdateTasks = true;
        //END POLICIES

        if( ! ($canUpdateTasks) ){
            return response()->json(['error' => 'Not authorized.'],403);
        }

        $task_id = $request->has('task') ? $request->input('task'): null;
        $new_priority = $request->has('priority') ? $request->input('priority'): null;

        try{
            $task_model = Task::where('id', $task_id)->first();
            $old_priority = $task_model->priority;

            $task_model->update([
                "priority" => $new_priority
            ]);

            DB::select("
                    UPDATE tasks SET
                        tasks.priority = 
                        CASE 
                        WHEN $new_priority < $old_priority AND tasks.priority < $old_priority THEN
                           tasks.priority + 1
                        WHEN $new_priority > $old_priority AND tasks.priority > $old_priority THEN
                           tasks.priority - 1
                        ELSE
                           tasks.priority
                        END               
                        WHERE  
                        tasks.id != $task_id AND
                        tasks.priority BETWEEN LEAST($new_priority, $old_priority) AND GREATEST($new_priority, $old_priority)
                ");

            return response()->json(['successful' => 'Updated'],200);
        }catch (\Throwable $e){
            return $e->getMessage();
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $taskRequest)
    {
        //POLICIES
        $canCreateTasks = true;
        //END POLICIES

        if(! $canCreateTasks){
            return back()->with('modalMessage',['Alert', __("You cannot create tasks.")]);
        }


        try {
            $task = Task::create($taskRequest->input());
            $priority = $taskRequest->input("priority");

            //UPDATE OTHER TASKS
            try {
                //UPDATE OTHER TASKS
                Task::
                    where('priority', '>=', $priority)
                    ->where('id', '!=', $task->id)
                    ->update([
                        'priority' => DB::raw('tasks.priority+1'),
                    ]);
            } catch(\Illuminate\Database\QueryException $e){
                //dd($e);
                return back()->with('modalMessage',['Alert',
                    __("Task was added, but an error occurred when updating others tasks.")]);

            }

            return back()->with('modalMessage',['Alert',
                __("Task added successfully.")]);
        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('modalMessage',['Aviso',
                __("Error adding task, please try again.")]);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\f  $f
     * @return \Illuminate\Http\Response
     */
    public function show(f $f)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\f  $f
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //POLICIES
        $canEditTasks = true;
        //END POLICIES

        if(! $canEditTasks){
            return response()->json(['error' => 'Not authorized.'],403);
        }

        $task_id = $request->has('task') ? $request->input('task'): null;

        try {
            $task = Task::where('id', $task_id)->first();
            $modalHTML = view('partials.modals.modalEditTask' , compact('task') )->render();
            return response()->json(['html' => $modalHTML],200);
        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return response()->json(['error' => 'Error retrieving the data.'],301);

        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\f  $f
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $taskRequest, Task $task)
    {
        //POLICIES
        $canEditTasks = true;
        //END POLICIES

        if(! $canEditTasks){
            return back()->with('modalMessage',['Alert', __("You cannot edit tasks.")]);
        }

        try {


            $task_id= $task->id;
            $old_priority= $task->priority;
            $task->fill($taskRequest->input())->save();
            $new_priority = $taskRequest->input("priority");

            //UPDATE OTHER TASKS
            try {
                //UPDATE OTHER TASKS

                DB::select("
                    UPDATE tasks SET
                        tasks.priority = 
                        CASE 
                        WHEN $new_priority < $old_priority AND tasks.priority < $old_priority THEN
                           tasks.priority + 1
                        WHEN $new_priority > $old_priority AND tasks.priority > $old_priority THEN
                           tasks.priority - 1
                        ELSE
                           tasks.priority
                        END               
                        WHERE  
                        tasks.id != $task_id AND
                        tasks.priority BETWEEN LEAST($new_priority, $old_priority) AND GREATEST($new_priority, $old_priority)
                ");

            } catch(\Illuminate\Database\QueryException $e){
                //dd($e);
                return back()->with('modalMessage',['Alert',
                    __("Task was updated, but an error occurred when updating others tasks.")]);

            }

            return back()->with('modalMessage',['Alert',
                __("Task updated successfully.")]);
        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('modalMessage',['Alert',
                __("Error updating task, please try again.")]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\f  $f
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //POLICIES
        //$canDeleteTasks = auth()->user()->can('update', Task::class );
        $canDeleteTasks = true;
        //END POLICIES

        if( ! ($canDeleteTasks) ){
            return response()->json(['error' => 'Not authorized.'],403);
        }

        try{

            $priority = $task->priority;
            $task->delete();

            //UPDATE OTHER TASKS
            try {
                //UPDATE OTHER TASKS
                Task::
                    where('priority', '>', $priority)
                    ->update([
                        'priority' => DB::raw('tasks.priority-1'),
                    ]);
            } catch(\Illuminate\Database\QueryException $e){
                //dd($e);
                return response()->json(['error' => 'Error updating the task priority.'],301);

            }

            return response()->json(['successful' => 'Updated'],200);
        }catch (\Throwable $e){
            //return $e->getMessage();

            return response()->json(['error' => 'Error deleting the task.'],301);
        }
    }
}
