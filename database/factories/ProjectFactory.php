<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {

    $project_company = $faker->company;
    $project_number = random_int(0, 10000);
    $project = "Project #".$project_number." for ".$project_company;

    return [
        'name' => $project,
    ];
});
