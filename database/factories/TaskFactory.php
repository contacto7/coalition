<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {

    $pre_arr=["Call", "Meeting with", "Dinner with", "Travel with"];
    $mid_arr=[" mom ", " dad ", " friend ", " brother "];
    $post_arr=[" on monday", " on tuesday", " on wednesday", " on thursday", " on friday", " on saturday", " on sunday"];

    $task = $faker->randomElement($pre_arr).$faker->randomElement($mid_arr).$faker->randomElement($post_arr);

    $due_to= \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '+2 days', $endDate = '+20 days')->getTimeStamp());

    return [
        'name' => $task,
        'priority' => $faker->numberBetween(1,10),
        'due_to' => $due_to,
        'project_id'=>\App\Project::all()->random()->id,
    ];
});
