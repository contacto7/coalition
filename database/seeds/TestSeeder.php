<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        //CREATE 3 PROJECTS
        factory(\App\Project::class, 3)->create()
            //FOR EACH PROJECT, CREATE 5 TASKS ASSOCIATED
            ->each(function (\App\Project $project) {
                //INITIAL TASK PRIORITY FOR PROJECT
                $task_priority = ($project->id - 1) * 5;
                for ($i = 1; $i <= 5; $i++) {
                    $task_priority++;
                    factory(\App\Task::class, 1)->create([
                        'project_id' => $project->id,
                        'priority' => $task_priority,
                    ]);
                }
        });

    }
}
