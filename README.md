## INSTALLING

- Create a folder for the project
- Initialize git: git init
- Add remote repository origin: git remote add origin https://gitlab.com/contacto7/coalition
- Pull from repository: git pull origin master
- Create a database for the project: heserleon 
- Create an .env file from the .env.example file in the root directory
- In the .env new file put the database information
- In the root directory, open terminal and execute the following code:
   - composer install
   - npm install
   - php artisan key:generate
   - composer dump-autoload
   - php artisan migrate:fresh --seed
   - php artisan storage:link

